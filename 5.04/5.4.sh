#!/bin/bash

PREFIX="${1:-NOT_SET}"
INTERFACE="$2"
SUBNET="$3"
HOST="$4"

re2='^(25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.(25[0-5]|(2[0-4]|1\d|[1-9]|)\d)$' #hmm, why dont want work correct WTF :)
re='^[0-9]{1,3}\.[0-9]{1,3}$'
reint='^([a-zA-Z].+)$'
resubnet='^(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)$'
rehost='^(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)$'


# -- print defaults arguments with micro help use script
function printdefaults() {
	echo "--------------------------------------------"
	echo "generate ip range net [x] aleksey martynenko"
	echo "for 5.4 lesson netologia course DevOps"
	echo "--------------------------------------------"
	echo "$0 <prefix> <interface> <subnet> <host>"
	echo "required: <prefix> <interface>"
	echo "optional: <subnet> <host>"
	echo "example: $0 192.168  enp8s0"
	echo "--------------------------------------------"
	echo "enjoy dude ;)"
}

# -- check range  count arguments
if [[ $# -lt 2 ]] || [[ $# -gt 4 ]];then 
	echo "error: count arguments illegal <2 or >4"
	printdefaults
	exit 1
fi

# -- logic
case $# in 
	2)
		if [[ "$PREFIX" =~ $re ]] &&  [[ "$INTERFACE" =~ $reint ]]; then 
		for SUBTMP in {1..255}
		do
			for HOSTMP in {1..255}
			do
				echo "[*] IP : ${PREFIX}.${SUBTMP}.${HOSTMP}"
				arping -c 3 -i "$INTERFACE" "${PREFIX}.${SUBTMP}.${HOSTMP}" 2> /dev/null
			done
		done
		else 
			echo "error: $PREFIX or $INTERFACE not correct format"
			printdefaults
			exit 1
		fi
		;;
	3)
		if [[ "$PREFIX" =~ $re ]] &&  [[ "$INTERFACE" =~ $reint ]] && [[ "$SUBNET" =~ $resubnet ]]; then 
				for HOSTMP in {1..255}
				do
					echo "[*] IP : ${PREFIX}.${SUBNET}.${HOSTMP}"
					arping -c 3 -i "$INTERFACE" "${PREFIX}.${SUBNET}.${HOSTMP}" 2> /dev/null
				done
			else 
				echo "error: $PREFIX or $INTERFACE or $SUBNET  not correct format"
				printdefaults
				exit 1
		fi
	;;
	4)
		if [[ "$PREFIX" =~ $re ]] &&  [[ "$INTERFACE" =~ $reint ]] && [[ "$SUBNET" =~ $resubnet ]] && [[ "$HOST" =~ $rehost ]]; then 
					echo "[*] IP : ${PREFIX}.${SUBNET}.${HOST}"
					arping -c 3 -i "$INTERFACE" "${PREFIX}.${SUBNET}.${HOST}" 2> /dev/null
			else 
			echo "error: $PREFIX or $INTERFACE or $SUBNET or $HOST  not correct format"
			printdefaults
			exit 1
		fi

	;;
esac
exit 0

